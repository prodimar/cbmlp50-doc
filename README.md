# CBMLP50: instrucciones para la actualización de firmware

## Consulta de la versión de firmware

El software de terminal de libre distribución **Hercules Setup** utilizado en estas capturas de pantalla puede descargarse [aquí](https://www.hw-group.com/software/hercules-setup-utility).

1. Conectar el dispositivo a un PC, vía USB, determinar en el Administrador de Dispositivos el puerto asignado y abrir dicho puerto con el terminal Hercules (la velocidad y demás parámetros del puerto no son significativos).

2. En el terminal Hercules (habilitar vista "_HEX Enable_") utilizando el botón derecho en pantalla principal.

3. Enviar comando [en **HEX**]:

    `1B 02 0F`

4.  El comando devuelve, en secuencia: 
    * Versión de hardware.
    * Versión de firmware.
    * Numero de serie del equipo.

En la captura de pantalla inferior:

    {01}{05}{31}{36}{34}{34}{30}{30}{32}{36}
     |   |  --------------------------------
     |   |   Número de serie (ASCII): 16440026
     |   |
     |   |__ Versión de firmware: 05
     |______ Versión de hardware: 01

![alt](https://bitbucket.org/prodimar/cbmlp50-doc/raw/master/media/screenshot.png "Determinación de la versión de firmware")

## Actualización de firmware

1. Desde el Hercules enviar comando [en HEX]

    `1B 02 0C`

2. Cerrar puerto COM   

3. El equipo se reenumerará como USB HID

4. Desde una línea de comandos **cmd** ejecutar el comando (ejecutable descargable de [aquí](https://bitbucket.org/prodimar/cbmlp50-doc/downloads/msp430-bsl5-flashfile.exe)):

        msp430-bsl5-flashfile.exe fichero_de_firmware
        Ejemplo: msp430-bsl5-flashfile.exe cbmlp50_v05_20210609.txt

![alt](https://bitbucket.org/prodimar/cbmlp50-doc/raw/master/media/screenshot_update_fw.png "Actualización de firmware")
